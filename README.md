![build](/../badges/master/build.svg)

Démonstration de l'utilisation du CAS de l'UTC pour _protéger_ des informations mises à disposition sur les comptes étu (wwwetu.utc.fr/~login).

`cas.php` est le fichier principal, il contient une classe `PHP` se chargeant de l'intéraction avec le CAS de l'UTC.

Pour l'utiliser vous pouvez suivre le fichier `test_login.php` :

```php
<?php
require_once("./cas.php");

$atr = CAS::authenticate();
if ($atr === false) {
    CAS::login();
} else {
    echo "Connexion validée, voici les attributs CAS : ";
    var_dump($atr);
}
```

Pour _protéger_ un fichier, en bash il suffit de faire : 
```bash
cat test_login_display.php > ./CAS_tests/protected.php
cat html_to_protect.html >> ./CAS_tests/protected.php
```

Puis de le mettre en ligne (exemple tiré du fichier deploy.sh -- ici tous les fichiers contenus dans le dossier `CAS_tests` généré dans `deploy.sh` sont transférés dans un dossier `CAS_tests` la racine du `public_html`) :

```bash
sshpass -p "$CAS_PASSWORD" scp -o StrictHostKeyChecking=no -r ./CAS_tests $CAS_LOGIN@stargate.utc.fr:~/public_html/
```

Le résultat est visible ici : [http://wwwetu.utc.fr/~chehabfl/CAS_tests/](http://wwwetu.utc.fr/~chehabfl/CAS_tests/).


NB : `$CAS_LOGIN` et `$CAS_PASSWORD` sont des variables d'environnements ajoutées lors des builds pour ce dépôt (pour des questions évidentes de sécurité...). Vous devez avoir affecté leurs valeurs respectives quand vous travaillez en local.