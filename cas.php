<?php
// BSD 2-Clause License

// Copyright (c) 2018, Florent Chehab
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.

// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


class CAS
{
    // UTC CAS URL
    const URL = 'https://cas.utc.fr/cas/';

    // Checks if connection is secure
    private static function isSecure()
    {
        return
            (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
            || $_SERVER['SERVER_PORT'] == 443;
    }

    // Base on isSecure function, see what it returns
    private static function http_s()
    {
        if (self::isSecure()) {
            return "https://";
        } else {
            return "http://";
        }
    }

    // Main function to authenticate a request
    public static function authenticate()
    {
        if (!isset($_GET['ticket']) || empty($_GET['ticket'])) {
            return false;
        }

        $service = self::http_s() . $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"], '?');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::URL . 'serviceValidate?service=' . $service . '&ticket=' . $_GET['ticket']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $data = curl_exec($curl);
        curl_close($curl);

        if (empty($data)) {
            return false;
        }

        // correct XML ... UTC WHAT IS THAT ?
        $data = str_replace("xmlns:cas='http://www.yale.edu/tp/cas'", "", $data);

        // remove namespace for easy handling with SimpleXMLElement
        $data = str_replace("<cas:", "<", $data);
        $data = str_replace("</cas:", "</", $data);
        $parsed = new SimpleXMLElement($data);

        $arr = (array) $parsed;

        if (!isset($arr["authenticationSuccess"])) {
            return false;
        }

        $attributes = (array) $arr["authenticationSuccess"]->attributes;
        $attributes['user'] = (string) $arr["authenticationSuccess"]->user;

        return $attributes;
    }

    // Function to perform login
    public static function login()
    {
        $service = self::http_s() . $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"], '?');
        header('Location: ' . self::URL . 'login?service=' . $service);
    }

    // Function to perform logout
    public static function logout()
    {
        header('Location: ' . self::URL . 'logout');
    }

}
