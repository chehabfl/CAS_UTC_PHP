<?php
require_once("./cas.php");

$atr = CAS::authenticate();
if ($atr === false) {
    CAS::login();
} else {
    echo "Connexion validée, voici les attributs CAS : ";
    var_dump($atr);
}
