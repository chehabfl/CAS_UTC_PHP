#!/bin/sh

rm -rf CAS_tests
mkdir CAS_tests

cat test_login_display.php > ./CAS_tests/protected.php
cat html_to_protect.html >> ./CAS_tests/protected.php

cp ./cas.php ./CAS_tests/cas.php 
cp ./test_login.php ./CAS_tests/test_login.php 

sshpass -p "$CAS_PASSWORD" scp -o StrictHostKeyChecking=no -r ./CAS_tests $CAS_LOGIN@stargate.utc.fr:~/public_html/
